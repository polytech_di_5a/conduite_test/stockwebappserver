package fr.polytech.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.polytech.stock.model.Store;

@Repository
public interface StoreRepository extends JpaRepository<Store,Long> {
    Store findById(long id);
}


