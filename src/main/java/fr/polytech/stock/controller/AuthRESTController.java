package fr.polytech.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import fr.polytech.stock.message.request.LoginForm;
import fr.polytech.stock.message.request.SignUpForm;
import fr.polytech.stock.message.response.JwtResponse;
import fr.polytech.stock.message.response.ResponseMessage;
import fr.polytech.stock.model.Department;
import fr.polytech.stock.model.Role;
import fr.polytech.stock.model.RoleName;
import fr.polytech.stock.model.Store;
import fr.polytech.stock.model.User;
import fr.polytech.stock.repository.DepartmentRepository;
import fr.polytech.stock.repository.RoleRepository;
import fr.polytech.stock.repository.StoreRepository;
import fr.polytech.stock.repository.UserRepository;
import fr.polytech.stock.security.jwt.JwtProvider;

import javax.validation.Valid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/restApi/auth")
public class AuthRESTController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtProvider jwtProvider;
    
    @Autowired
    StoreRepository storeRepository;
    
    @Autowired
    DepartmentRepository departmentRepository;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) ((org.springframework.security.core.Authentication) authentication).getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt,userDetails.getUsername(), userDetails.getAuthorities()));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken."), HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken."), HttpStatus.BAD_REQUEST);
        }
        DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
        try
        {
           df.parse(signUpRequest.getBirthday());
        }
        catch(Exception e)
        {            
            return new ResponseEntity<>(new ResponseMessage("Fail -> Date format required like dd/mm/yyyy."), HttpStatus.BAD_REQUEST);
        }

        // Create user account
        User user = new User(signUpRequest.getUsername(), passwordEncoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
            switch (role) {
                case "admin":
                    Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Fail -> Cause: Admin Role not found."));
                    roles.add(adminRole);
                    break;
                default:
                    Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException("Fail -> Cause: User Role not found."));
                    roles.add(userRole);
            }
        });

        user.setRoles(roles);
        user.setBirthday(signUpRequest.getBirthday());
        userRepository.save(user);

        return new ResponseEntity<>(new ResponseMessage("User registered successfully."), HttpStatus.OK);

    }
    
    @RequestMapping(method = RequestMethod.GET)
    public List<User> findAllUsers() {
        List<User> users = userRepository.findAll();
        List<User> userlist = new ArrayList<User>(users);
        List<Store> stores = storeRepository.findAll();
        List<Department> departments = departmentRepository.findAll();
        for(User user : users) {
            for(Role role : user.getRoles()) {
                if(role.getName()!=RoleName.ROLE_USER)
                    userlist.remove(user);
            }
        }
        for(User user : users) {
            for(Store store : stores) {
                if(store.getUser()==user)
                    userlist.remove(user);
            }
        }
        for(User user : users) {
            for(Department department : departments) {
                if(department.getUser()==user)
                    userlist.remove(user);
            }
        }
        return userlist;
    }
}
