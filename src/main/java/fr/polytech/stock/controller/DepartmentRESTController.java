package fr.polytech.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.polytech.stock.model.Department;
import fr.polytech.stock.repository.DepartmentRepository;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/restApi/departments")
public class DepartmentRESTController {

    private DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentRESTController(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    //get All
    @RequestMapping(method = RequestMethod.GET/*, produces = "application/xml"*/)
    //@GetMapping
    public List<Department> findAllDepartments() {
        return departmentRepository.findAll();
    }
    
    //get By Id
    @RequestMapping(value="/{id}", method = RequestMethod.GET/*, produces = "application/xml"*/)
    //@GetMapping
    public Department findDepartment(@PathVariable("id") long id) {
        return departmentRepository.findById(id);
    }
    
    //Create new entry
    @RequestMapping(method = RequestMethod.POST)
    //@PostMapping
    public ResponseEntity<Department> addDepartment(@RequestBody Department department) {
        departmentRepository.save(department);
        return new ResponseEntity<Department>(department, HttpStatus.CREATED);
    }

    //Delete By Id
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    //@DeleteMapping("/{id}")
    public ResponseEntity<Department> deleteDepartment (@PathVariable("id") long id) {
        Department department = departmentRepository.findById(id);
        if (department == null) {
            System.out.println("Department not found!");
            return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
        }

        departmentRepository.deleteById(id);
        return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
    }
    
    //Delete All
    @RequestMapping(method = RequestMethod.DELETE)
    //@DeleteMapping
    public ResponseEntity<Department> deleteAllDepartment () {
        List<Department> departments = departmentRepository.findAll();
        if (departments.isEmpty()!=true) {
            System.out.println("Departments not found!");
            return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
        }

        departmentRepository.deleteAll();
        return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
    }
    
    //ReplaceAll
    @RequestMapping(method = RequestMethod.PUT)
    //@PutMapping
    public ResponseEntity<Department> updateDepartment(@RequestBody List<Department> departments) {
        List<Department> oldDepartments = departmentRepository.findAll();
        int i=0;
       for(Department c : departments){
           if(i<oldDepartments.size()) {
               c.setId(oldDepartments.get(i).getId());
               i++;
           }
           departmentRepository.save(c);
        }
        return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
    }

    //Replace By Id
    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    //@PutMapping("/{id}")
    public ResponseEntity<Department> updateDepartment(@RequestBody Department department, @PathVariable("id") long id) {
        department.setId(id);
        departmentRepository.save(department);
        return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
    }

    //Update By Id
    @RequestMapping(value="/{id}", method = RequestMethod.PATCH)
    //@PatchMapping("/{id}")
    public ResponseEntity<Department> updatePartOfDepartment(@RequestBody Map<String, Object> updates, @PathVariable("id") long id) {
        Department department = departmentRepository.findById(id);
        if (department == null) {
            System.out.println("Department not found!");
            return new ResponseEntity<Department>(HttpStatus.NOT_FOUND);
        }
        partialUpdate(department,updates);
        return new ResponseEntity<Department>(HttpStatus.NO_CONTENT);
    }

    //Partial update
    private void partialUpdate(Department department, Map<String, Object> updates) {
        if (updates.containsKey("name")) {
            department.setName((String) updates.get("name"));
        }
        departmentRepository.save(department);
    }

}


